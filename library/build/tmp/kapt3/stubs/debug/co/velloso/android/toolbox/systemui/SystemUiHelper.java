package co.velloso.android.toolbox.systemui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010\u0010\u001a\u00020\u0011H\u0002J\u0006\u0010\u0012\u001a\u00020\u0011J\u0006\u0010\u0013\u001a\u00020\u0011J\u0006\u0010\u0014\u001a\u00020\u0011J\u0006\u0010\u0015\u001a\u00020\u0011R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lco/velloso/android/toolbox/systemui/SystemUiHelper;", "", "window", "Landroid/view/Window;", "container", "Landroid/view/View;", "handler", "Landroid/os/Handler;", "(Landroid/view/Window;Landroid/view/View;Landroid/os/Handler;)V", "hiddenNotificationVisibility", "", "hiddenSystemUiVisibility", "mHiddenNotificationRunnable", "Ljava/lang/Runnable;", "mHiddenSystemUiVisibility", "originalSystemUiVisibility", "clearAllFlags", "", "hideNotification", "hideStatusBar", "hideSystemUi", "showSystemUi", "toolbox_debug"})
public final class SystemUiHelper {
    private final int originalSystemUiVisibility = 0;
    private final int hiddenNotificationVisibility = 0;
    private final int hiddenSystemUiVisibility = 4610;
    private final java.lang.Runnable mHiddenNotificationRunnable = null;
    private final java.lang.Runnable mHiddenSystemUiVisibility = null;
    private final android.view.Window window = null;
    private final android.view.View container = null;
    private final android.os.Handler handler = null;
    
    private final void clearAllFlags() {
    }
    
    public final void showSystemUi() {
    }
    
    public final void hideNotification() {
    }
    
    public final void hideStatusBar() {
    }
    
    public final void hideSystemUi() {
    }
    
    public SystemUiHelper(@org.jetbrains.annotations.NotNull()
    android.view.Window window, @org.jetbrains.annotations.NotNull()
    android.view.View container, @org.jetbrains.annotations.NotNull()
    android.os.Handler handler) {
        super();
    }
}
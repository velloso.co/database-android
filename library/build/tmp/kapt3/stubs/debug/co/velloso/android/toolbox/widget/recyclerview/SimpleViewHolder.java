package co.velloso.android.toolbox.widget.recyclerview;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0016\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lco/velloso/android/toolbox/widget/recyclerview/SimpleViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "parent", "Landroid/view/ViewGroup;", "layoutRes", "", "(Landroid/view/ViewGroup;I)V", "toolbox_debug"})
public class SimpleViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
    
    public SimpleViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, @androidx.annotation.LayoutRes()
    int layoutRes) {
        super(null);
    }
}
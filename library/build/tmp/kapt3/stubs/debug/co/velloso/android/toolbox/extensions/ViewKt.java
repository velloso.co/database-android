package co.velloso.android.toolbox.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000e\u001a*\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0004\u001a?\u0010\b\u001a\u00020\u0001*\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\r\u001a\'\u0010\u000e\u001a\u00020\u0001*\u00020\u00022\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0011\u00a8\u0006\u0012"}, d2 = {"setMarginInFrameLayout", "", "Landroid/view/View;", "left", "", "top", "right", "bottom", "setPaddingDimen", "dimenLeft", "dimenTop", "dimenRight", "dimenBottom", "(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V", "setSize", "width", "height", "(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;)V", "toolbox_debug"})
public final class ViewKt {
    
    /**
     * * Android Views extensions
     */
    public static final void setSize(@org.jetbrains.annotations.NotNull()
    android.view.View $receiver, @org.jetbrains.annotations.Nullable()
    java.lang.Integer width, @org.jetbrains.annotations.Nullable()
    java.lang.Integer height) {
    }
    
    public static final void setMarginInFrameLayout(@org.jetbrains.annotations.NotNull()
    android.view.View $receiver, int left, int top, int right, int bottom) {
    }
    
    public static final void setPaddingDimen(@org.jetbrains.annotations.NotNull()
    android.view.View $receiver, @org.jetbrains.annotations.Nullable()
    java.lang.Integer dimenLeft, @org.jetbrains.annotations.Nullable()
    java.lang.Integer dimenTop, @org.jetbrains.annotations.Nullable()
    java.lang.Integer dimenRight, @org.jetbrains.annotations.Nullable()
    java.lang.Integer dimenBottom) {
    }
}
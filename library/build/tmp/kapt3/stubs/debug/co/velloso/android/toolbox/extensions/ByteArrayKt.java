package co.velloso.android.toolbox.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000.\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u0005\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0005\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u0016\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\n\u0010\u0004\u001a\u00020\u0001\"\u00020\u0003\u001a\n\u0010\u0005\u001a\u00020\u0006*\u00020\u0001\u001a\n\u0010\u0007\u001a\u00020\u0001*\u00020\u0001\u001a\"\u0010\b\u001a\u00020\u0006*\u00020\u00012\u0006\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u0006\u001a\u0012\u0010\f\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u0014\u0010\r\u001a\u00020\u0001*\u00020\u00012\b\b\u0002\u0010\u000e\u001a\u00020\u0006\u001a\u001a\u0010\u000f\u001a\u00020\u0010*\u00020\u00012\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0012\u001a\u0018\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00010\u0015*\u00020\u00012\u0006\u0010\u0016\u001a\u00020\u0003\u001a\u0018\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00010\u0015*\u00020\u00012\u0006\u0010\u0016\u001a\u00020\u0001\u001a\n\u0010\u0017\u001a\u00020\u0012*\u00020\u0001\u001a\u0012\u0010\u0018\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0019\u001a\u00020\u0006\u00a8\u0006\u001a"}, d2 = {"append", "", "byte", "", "elements", "checkSum", "", "compress", "crc16", "polynomial", "initialValue", "finalXorValue", "prepend", "removeLast", "last", "saveFile", "", "path", "", "filename", "split", "", "delimiter", "toHexString", "uncompress", "bufferSize", "toolbox_debug"})
public final class ByteArrayKt {
    
    /**
     * * converts the given String to CRC16
     * *
     * * @param polynomial
     * * - the polynomial (divisor)
     * * @param initialValue
     * * - the initial CRC value
     * * @param finalXorValue
     * * - the value that the crc should be applied with a xor bitwise operation
     * * ASCII
     * * @return
     */
    public static final int crc16(@org.jetbrains.annotations.NotNull()
    byte[] $receiver, int polynomial, int initialValue, int finalXorValue) {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final byte[] uncompress(@org.jetbrains.annotations.NotNull()
    byte[] $receiver, int bufferSize) throws java.io.IOException, java.util.zip.DataFormatException {
        return null;
    }
    
    public static final int checkSum(@org.jetbrains.annotations.NotNull()
    byte[] $receiver) {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final byte[] compress(@org.jetbrains.annotations.NotNull()
    byte[] $receiver) throws java.io.IOException {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.List<byte[]> split(@org.jetbrains.annotations.NotNull()
    byte[] $receiver, @org.jetbrains.annotations.NotNull()
    byte[] delimiter) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.List<byte[]> split(@org.jetbrains.annotations.NotNull()
    byte[] $receiver, byte delimiter) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final byte[] removeLast(@org.jetbrains.annotations.NotNull()
    byte[] $receiver, int last) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final byte[] prepend(@org.jetbrains.annotations.NotNull()
    byte[] $receiver, byte p1_1519748) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final byte[] append(@org.jetbrains.annotations.NotNull()
    byte[] $receiver, byte p1_1519748) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final byte[] append(@org.jetbrains.annotations.NotNull()
    byte[] $receiver, @org.jetbrains.annotations.NotNull()
    byte... elements) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toHexString(@org.jetbrains.annotations.NotNull()
    byte[] $receiver) {
        return null;
    }
    
    public static final void saveFile(@org.jetbrains.annotations.NotNull()
    byte[] $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.String path, @org.jetbrains.annotations.NotNull()
    java.lang.String filename) throws java.io.IOException {
    }
}
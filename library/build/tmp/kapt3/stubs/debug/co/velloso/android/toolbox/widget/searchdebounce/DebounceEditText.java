package co.velloso.android.toolbox.widget.searchdebounce;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001&B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010\u0019\u001a\u00020\u001aH\u0014J\b\u0010\u001b\u001a\u00020\u001aH\u0014J+\u0010\u001c\u001a\u00020\u001a2#\u0010\f\u001a\u001f\u0012\u0013\u0012\u00110\u001e\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(!\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u001dJ\u0018\u0010\"\u001a\u00020\u001a2\b\u0010#\u001a\u0004\u0018\u00010\u001e2\u0006\u0010$\u001a\u00020%R\u000e\u0010\t\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001a\u0010\u0013\u001a\u00020\u0014X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018\u00a8\u0006\'"}, d2 = {"Lco/velloso/android/toolbox/widget/searchdebounce/DebounceEditText;", "Lcom/google/android/material/textfield/TextInputEditText;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "debounceCounter", "disposable", "Lio/reactivex/disposables/Disposable;", "listener", "Lco/velloso/android/toolbox/widget/searchdebounce/DebounceEditText$SearchDebounceViewListener;", "maxDebounce", "getMaxDebounce", "()I", "setMaxDebounce", "(I)V", "timeDebounce", "", "getTimeDebounce", "()J", "setTimeDebounce", "(J)V", "onAttachedToWindow", "", "onDetachedFromWindow", "setOnDebouncedQueryListener", "Lkotlin/Function1;", "", "Lkotlin/ParameterName;", "name", "queryText", "submitQuery", "query", "submit", "", "SearchDebounceViewListener", "toolbox_debug"})
public final class DebounceEditText extends com.google.android.material.textfield.TextInputEditText {
    private int maxDebounce;
    private long timeDebounce;
    private io.reactivex.disposables.Disposable disposable;
    private int debounceCounter;
    private co.velloso.android.toolbox.widget.searchdebounce.DebounceEditText.SearchDebounceViewListener listener;
    private java.util.HashMap _$_findViewCache;
    
    public final int getMaxDebounce() {
        return 0;
    }
    
    public final void setMaxDebounce(int p0) {
    }
    
    public final long getTimeDebounce() {
        return 0L;
    }
    
    public final void setTimeDebounce(long p0) {
    }
    
    @java.lang.Override()
    protected void onAttachedToWindow() {
    }
    
    @java.lang.Override()
    protected void onDetachedFromWindow() {
    }
    
    /**
     * * Sets a query string in the text field and optionally submits the query as well.
     *     *
     *     * @param submit if true, immediately call onDebouncedQuery listener
     */
    public final void submitQuery(@org.jetbrains.annotations.Nullable()
    java.lang.CharSequence query, boolean submit) {
    }
    
    public final void setOnDebouncedQueryListener(@org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function1<? super java.lang.CharSequence, kotlin.Unit> listener) {
    }
    
    public DebounceEditText(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs, int defStyleAttr) {
        super(null);
    }
    
    public DebounceEditText(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs) {
        super(null);
    }
    
    public DebounceEditText(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\r\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lco/velloso/android/toolbox/widget/searchdebounce/DebounceEditText$SearchDebounceViewListener;", "", "onDebouncedQuery", "", "queryText", "", "toolbox_debug"})
    public static abstract interface SearchDebounceViewListener {
        
        public abstract void onDebouncedQuery(@org.jetbrains.annotations.NotNull()
        java.lang.CharSequence queryText);
    }
}
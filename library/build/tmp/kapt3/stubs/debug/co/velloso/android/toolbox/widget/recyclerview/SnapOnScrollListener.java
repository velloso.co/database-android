package co.velloso.android.toolbox.widget.recyclerview;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 !2\u00020\u0001:\u0001!BK\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012:\b\u0002\u0010\u0004\u001a4\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\f\b\u0007\u0012\b\b\b\u0012\u0004\b\b(\t\u0012\u0013\u0012\u00110\n\u00a2\u0006\f\b\u0007\u0012\b\b\b\u0012\u0004\b\b(\u000b\u0012\u0004\u0012\u00020\f\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\rBS\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u000e\u001a\u00020\n\u0012:\b\u0002\u0010\u0004\u001a4\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\f\b\u0007\u0012\b\b\b\u0012\u0004\b\b(\t\u0012\u0013\u0012\u00110\n\u00a2\u0006\f\b\u0007\u0012\b\b\b\u0012\u0004\b\b(\u000b\u0012\u0004\u0012\u00020\f\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0019\u001a\u00020\f2\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0018\u0010\u001c\u001a\u00020\f2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\nH\u0016J \u0010\u001e\u001a\u00020\f2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020\n2\u0006\u0010 \u001a\u00020\nH\u0016R\u001a\u0010\u000e\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013RL\u0010\u0004\u001a4\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\f\b\u0007\u0012\b\b\b\u0012\u0004\b\b(\t\u0012\u0013\u0012\u00110\n\u00a2\u0006\f\b\u0007\u0012\b\b\b\u0012\u0004\b\b(\u000b\u0012\u0004\u0012\u00020\f\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""}, d2 = {"Lco/velloso/android/toolbox/widget/recyclerview/SnapOnScrollListener;", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "snapHelper", "Landroidx/recyclerview/widget/SnapHelper;", "onSnapPositionChangeListener", "Lkotlin/Function2;", "Landroid/view/View;", "Lkotlin/ParameterName;", "name", "v", "", "position", "", "(Landroidx/recyclerview/widget/SnapHelper;Lkotlin/jvm/functions/Function2;)V", "behavior", "(Landroidx/recyclerview/widget/SnapHelper;ILkotlin/jvm/functions/Function2;)V", "getBehavior", "()I", "setBehavior", "(I)V", "getOnSnapPositionChangeListener", "()Lkotlin/jvm/functions/Function2;", "setOnSnapPositionChangeListener", "(Lkotlin/jvm/functions/Function2;)V", "snapPosition", "dispatchSnapPositionChange", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "onScrollStateChanged", "newState", "onScrolled", "dx", "dy", "Companion", "toolbox_debug"})
public final class SnapOnScrollListener extends androidx.recyclerview.widget.RecyclerView.OnScrollListener {
    private int snapPosition;
    private final androidx.recyclerview.widget.SnapHelper snapHelper = null;
    private int behavior;
    @org.jetbrains.annotations.Nullable()
    private kotlin.jvm.functions.Function2<? super android.view.View, ? super java.lang.Integer, kotlin.Unit> onSnapPositionChangeListener;
    public static final int NOTIFY_ON_SCROLL = 0;
    public static final int NOTIFY_ON_SCROLL_STATE_IDLE = 1;
    public static final co.velloso.android.toolbox.widget.recyclerview.SnapOnScrollListener.Companion Companion = null;
    
    @java.lang.Override()
    public void onScrolled(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView recyclerView, int dx, int dy) {
    }
    
    @java.lang.Override()
    public void onScrollStateChanged(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView recyclerView, int newState) {
    }
    
    private final void dispatchSnapPositionChange(androidx.recyclerview.widget.RecyclerView recyclerView) {
    }
    
    public final int getBehavior() {
        return 0;
    }
    
    public final void setBehavior(int p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final kotlin.jvm.functions.Function2<android.view.View, java.lang.Integer, kotlin.Unit> getOnSnapPositionChangeListener() {
        return null;
    }
    
    public final void setOnSnapPositionChangeListener(@org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function2<? super android.view.View, ? super java.lang.Integer, kotlin.Unit> p0) {
    }
    
    public SnapOnScrollListener(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.SnapHelper snapHelper, int behavior, @org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function2<? super android.view.View, ? super java.lang.Integer, kotlin.Unit> onSnapPositionChangeListener) {
        super();
    }
    
    public SnapOnScrollListener(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.SnapHelper snapHelper, @org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function2<? super android.view.View, ? super java.lang.Integer, kotlin.Unit> onSnapPositionChangeListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lco/velloso/android/toolbox/widget/recyclerview/SnapOnScrollListener$Companion;", "", "()V", "NOTIFY_ON_SCROLL", "", "NOTIFY_ON_SCROLL_STATE_IDLE", "toolbox_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}
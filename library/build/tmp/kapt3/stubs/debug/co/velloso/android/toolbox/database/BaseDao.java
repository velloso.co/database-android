package co.velloso.android.toolbox.database;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0007\b\'\u0018\u0000 \u0018*\u0006\b\u0000\u0010\u0001 \u00002\u00020\u0002:\u0001\u0018B\u0005\u00a2\u0006\u0002\u0010\u0003J!\u0010\u0004\u001a\u00020\u00052\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u0000H\'\u00a2\u0006\u0002\u0010\bJD\u0010\u0004\u001a\u00020\t2\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u00002#\u0010\n\u001a\u001f\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u00020\t0\u000b\u00a2\u0006\u0002\u0010\u000fJ\'\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u0000H\'\u00a2\u0006\u0002\u0010\u0013JJ\u0010\u0010\u001a\u00020\t2\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u00002)\u0010\n\u001a%\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u0011\u00a2\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u0014\u0012\u0004\u0012\u00020\t0\u000b\u00a2\u0006\u0002\u0010\u000fJ!\u0010\u0015\u001a\u00020\u00052\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u0000H\'\u00a2\u0006\u0002\u0010\bJD\u0010\u0015\u001a\u00020\t2\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u00002#\u0010\n\u001a\u001f\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\t0\u000b\u00a2\u0006\u0002\u0010\u000fJ)\u0010\u0017\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u00112\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u0000H\u0017\u00a2\u0006\u0002\u0010\u0013JL\u0010\u0017\u001a\u00020\t2\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u00002+\u0010\n\u001a\'\u0012\u001d\u0012\u001b\u0012\u0006\u0012\u0004\u0018\u00010\u0012\u0018\u00010\u0011\u00a2\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u0014\u0012\u0004\u0012\u00020\t0\u000b\u00a2\u0006\u0002\u0010\u000f\u00a8\u0006\u0019"}, d2 = {"Lco/velloso/android/toolbox/database/BaseDao;", "T", "", "()V", "delete", "", "entities", "", "([Ljava/lang/Object;)I", "", "result", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "rowsDeleted", "([Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V", "insert", "", "", "([Ljava/lang/Object;)Ljava/util/List;", "ids", "update", "rowsUpdated", "upsert", "Companion", "toolbox_debug"})
public abstract class BaseDao<T extends java.lang.Object> {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TAG = "BaseDao";
    public static final co.velloso.android.toolbox.database.BaseDao.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract java.util.List<java.lang.Long> insert(@org.jetbrains.annotations.NotNull()
    T... entities);
    
    @androidx.room.Update()
    public abstract int update(@org.jetbrains.annotations.NotNull()
    T... entities);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Transaction()
    public java.util.List<java.lang.Long> upsert(@org.jetbrains.annotations.NotNull()
    T... entities) {
        return null;
    }
    
    @androidx.room.Delete()
    public abstract int delete(@org.jetbrains.annotations.NotNull()
    T... entities);
    
    public final void insert(@org.jetbrains.annotations.NotNull()
    T[] entities, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.util.List<java.lang.Long>, kotlin.Unit> result) {
    }
    
    public final void upsert(@org.jetbrains.annotations.NotNull()
    T[] entities, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.util.List<java.lang.Long>, kotlin.Unit> result) {
    }
    
    public final void update(@org.jetbrains.annotations.NotNull()
    T[] entities, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> result) {
    }
    
    public final void delete(@org.jetbrains.annotations.NotNull()
    T[] entities, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> result) {
    }
    
    public BaseDao() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lco/velloso/android/toolbox/database/BaseDao$Companion;", "", "()V", "TAG", "", "toolbox_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}
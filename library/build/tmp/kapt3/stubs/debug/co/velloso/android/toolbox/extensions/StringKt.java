package co.velloso.android.toolbox.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u00008\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0012\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0000\u001a\u001d\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005\u001a\n\u0010\u0006\u001a\u00020\u0002*\u00020\u0002\u001a\n\u0010\u0007\u001a\u00020\u0002*\u00020\u0002\u001a\n\u0010\b\u001a\u00020\u0002*\u00020\u0002\u001a\n\u0010\t\u001a\u00020\u0002*\u00020\u0002\u001a\n\u0010\n\u001a\u00020\u000b*\u00020\u0002\u001a\u0012\u0010\f\u001a\u00020\u0004*\u00020\u00022\u0006\u0010\r\u001a\u00020\u0002\u001a\"\u0010\u000e\u001a\u00020\u0004*\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0004\u001a\n\u0010\u0012\u001a\u00020\u0002*\u00020\u0002\u001a\n\u0010\u0013\u001a\u00020\u0002*\u00020\u0002\u001a\n\u0010\u0014\u001a\u00020\u0002*\u00020\u0002\u001a\n\u0010\u0015\u001a\u00020\u0002*\u00020\u0002\u001a\n\u0010\u0016\u001a\u00020\u0002*\u00020\u0002\u001a\n\u0010\u0017\u001a\u00020\u0002*\u00020\u0002\u001a\n\u0010\u0018\u001a\u00020\u000b*\u00020\u0002\u001a\n\u0010\u0019\u001a\u00020\u001a*\u00020\u0002\u001a\u0016\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u001c*\u00020\u0002\u001a\n\u0010\u001d\u001a\u00020\u0002*\u00020\u0002\u001a\n\u0010\u001e\u001a\u00020\u0002*\u00020\u0002\u001a\n\u0010\u001f\u001a\u00020\u0002*\u00020\u0002\u001a\n\u0010 \u001a\u00020\u0002*\u00020\u0002\u001a\"\u0010!\u001a\u00020\"*\u00020\u00022\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010$\u001a\u00020\u0002H\u0007\u001a\n\u0010%\u001a\u00020&*\u00020\u0002\u00a8\u0006\'"}, d2 = {"chunked", "", "", "size", "", "(Ljava/lang/String;I)[Ljava/lang/String;", "cleanCnpj", "cleanCpf", "cleanPhone", "cleanZipCode", "compress", "", "countOccurrences", "occurrence", "crc16", "polynomial", "initialValue", "finalXorValue", "formatCep", "formatCnpj", "formatCpf", "formatCreditCard", "formatTaxPayerId", "formatValidThrough", "hexStringToByteArray", "isNumeric", "", "macAddressToCompositeId", "Lkotlin/Pair;", "md5", "onlyNumeric", "removeAccent", "toBarcodeString", "toDate", "Ljava/util/Date;", "pattern", "locale", "toDoubleLocale", "", "toolbox_debug"})
public final class StringKt {
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"SimpleDateFormat"})
    public static final java.util.Date toDate(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver, @org.jetbrains.annotations.Nullable()
    java.lang.String pattern, @org.jetbrains.annotations.NotNull()
    java.lang.String locale) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toBarcodeString(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final byte[] compress(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) throws java.io.IOException {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String[] chunked(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver, int size) {
        return null;
    }
    
    /**
     * * Cryptography string to MD5
     */
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String md5(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    public static final int crc16(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver, int polynomial, int initialValue, int finalXorValue) {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final byte[] hexStringToByteArray(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String removeAccent(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final kotlin.Pair<java.lang.Integer, java.lang.Integer> macAddressToCompositeId(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String onlyNumeric(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    public static final int countOccurrences(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.String occurrence) {
        return 0;
    }
    
    public static final boolean isNumeric(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return false;
    }
    
    public static final double toDoubleLocale(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return 0.0;
    }
    
    /**
     * *
     */
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String formatCep(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String formatCreditCard(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String formatValidThrough(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String cleanCpf(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String cleanCnpj(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String cleanPhone(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String cleanZipCode(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String formatTaxPayerId(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String formatCpf(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String formatCnpj(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return null;
    }
}
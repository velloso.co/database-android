package co.velloso.android.toolbox.behaviors;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u000e\u0010\u0000\u001a\u0006\u0012\u0002\b\u00030\u0001*\u00020\u0002\u001a\u000e\u0010\u0003\u001a\u0006\u0012\u0002\b\u00030\u0004*\u00020\u0002\u00a8\u0006\u0005"}, d2 = {"getBehavior", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "Landroid/view/View;", "getLockableBehavior", "Lco/velloso/android/toolbox/behaviors/LockableBottomSheetBehavior;", "toolbox_debug"})
public final class ViewKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final com.google.android.material.bottomsheet.BottomSheetBehavior<?> getBehavior(@org.jetbrains.annotations.NotNull()
    android.view.View $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final co.velloso.android.toolbox.behaviors.LockableBottomSheetBehavior<?> getLockableBehavior(@org.jetbrains.annotations.NotNull()
    android.view.View $receiver) {
        return null;
    }
}
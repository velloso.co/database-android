package co.velloso.android.toolbox.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0002\u0010\u000b\n\u0002\b\u0003\u001a\u001a\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0001\u00a8\u0006\u0005"}, d2 = {"toStringYesNo", "", "", "yesString", "noString", "toolbox_debug"})
public final class BooleanKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toStringYesNo(boolean $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.String yesString, @org.jetbrains.annotations.NotNull()
    java.lang.String noString) {
        return null;
    }
}
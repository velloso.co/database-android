package co.velloso.android.toolbox.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0014\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a7\u0010\u0000\u001a\u0002H\u0001\"\b\b\u0000\u0010\u0001*\u00020\u00022\b\u0010\u0003\u001a\u0004\u0018\u0001H\u00012\b\u0010\u0004\u001a\u0004\u0018\u0001H\u00012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00020\u0006\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\b"}, d2 = {"checkDiff", "T", "", "value", "secondValue", "lazyMessage", "Lkotlin/Function0;", "(Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "toolbox_debug"})
public final class AnyKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final <T extends java.lang.Object>T checkDiff(@org.jetbrains.annotations.Nullable()
    T value, @org.jetbrains.annotations.Nullable()
    T secondValue, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<? extends java.lang.Object> lazyMessage) {
        return null;
    }
}
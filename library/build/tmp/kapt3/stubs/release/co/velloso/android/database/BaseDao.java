package co.velloso.android.database;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\t\b\'\u0018\u0000 \u0019*\u0006\b\u0000\u0010\u0001 \u00002\u00020\u0002:\u0002\u0019\u001aB\u0005\u00a2\u0006\u0002\u0010\u0003J!\u0010\u0004\u001a\u00020\u00052\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u0000H\'\u00a2\u0006\u0002\u0010\bJD\u0010\u0004\u001a\u00020\t2\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u00002#\u0010\n\u001a\u001f\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u00020\t0\u000b\u00a2\u0006\u0002\u0010\u000fJ\'\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u0000H\'\u00a2\u0006\u0002\u0010\u0013JJ\u0010\u0010\u001a\u00020\t2\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u00002)\u0010\n\u001a%\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u0011\u00a2\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u0014\u0012\u0004\u0012\u00020\t0\u000b\u00a2\u0006\u0002\u0010\u000fJ)\u0010\u0015\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u00112\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u0000H\u0017\u00a2\u0006\u0002\u0010\u0013JL\u0010\u0015\u001a\u00020\t2\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u00002+\u0010\n\u001a\'\u0012\u001d\u0012\u001b\u0012\u0006\u0012\u0004\u0018\u00010\u0012\u0018\u00010\u0011\u00a2\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u0014\u0012\u0004\u0012\u00020\t0\u000b\u00a2\u0006\u0002\u0010\u000fJ!\u0010\u0016\u001a\u00020\u00052\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u0000H\'\u00a2\u0006\u0002\u0010\bJD\u0010\u0016\u001a\u00020\t2\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u00002#\u0010\n\u001a\u001f\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u0017\u0012\u0004\u0012\u00020\t0\u000b\u00a2\u0006\u0002\u0010\u000fJ)\u0010\u0018\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u00112\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u0000H\u0017\u00a2\u0006\u0002\u0010\u0013JL\u0010\u0018\u001a\u00020\t2\u0012\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0007\"\u00028\u00002+\u0010\n\u001a\'\u0012\u001d\u0012\u001b\u0012\u0006\u0012\u0004\u0018\u00010\u0012\u0018\u00010\u0011\u00a2\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u0014\u0012\u0004\u0012\u00020\t0\u000b\u00a2\u0006\u0002\u0010\u000f\u00a8\u0006\u001b"}, d2 = {"Lco/velloso/android/database/BaseDao;", "T", "", "()V", "delete", "", "entities", "", "([Ljava/lang/Object;)I", "", "result", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "rowsDeleted", "([Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V", "insert", "", "", "([Ljava/lang/Object;)Ljava/util/List;", "ids", "save", "update", "rowsUpdated", "upsert", "Companion", "SimpleAsyncTask", "library_release"})
public abstract class BaseDao<T extends java.lang.Object> {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TAG = "BaseDao";
    public static final co.velloso.android.database.BaseDao.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract java.util.List<java.lang.Long> insert(@org.jetbrains.annotations.NotNull()
    T... entities);
    
    @androidx.room.Update()
    public abstract int update(@org.jetbrains.annotations.NotNull()
    T... entities);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Transaction()
    public java.util.List<java.lang.Long> save(@org.jetbrains.annotations.NotNull()
    T... entities) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Transaction()
    public java.util.List<java.lang.Long> upsert(@org.jetbrains.annotations.NotNull()
    T... entities) {
        return null;
    }
    
    @androidx.room.Delete()
    public abstract int delete(@org.jetbrains.annotations.NotNull()
    T... entities);
    
    public final void insert(@org.jetbrains.annotations.NotNull()
    T[] entities, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.util.List<java.lang.Long>, kotlin.Unit> result) {
    }
    
    public final void save(@org.jetbrains.annotations.NotNull()
    T[] entities, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.util.List<java.lang.Long>, kotlin.Unit> result) {
    }
    
    public final void upsert(@org.jetbrains.annotations.NotNull()
    T[] entities, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.util.List<java.lang.Long>, kotlin.Unit> result) {
    }
    
    public final void update(@org.jetbrains.annotations.NotNull()
    T[] entities, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> result) {
    }
    
    public final void delete(@org.jetbrains.annotations.NotNull()
    T[] entities, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> result) {
    }
    
    public BaseDao() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\u0011\n\u0002\b\u0004\b\u0002\u0018\u0000*\u0004\b\u0001\u0010\u00012\u0016\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u0001H\u00010\u0002BU\u0012\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00010\u0005\u0012>\b\u0002\u0010\u0006\u001a8\u0012\u0015\u0012\u0013\u0018\u00018\u0001\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0015\u0012\u0013\u0018\u00010\u000b\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u00020\r\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u000eJ\'\u0010\u0013\u001a\u0004\u0018\u00018\u00012\u0016\u0010\u0014\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u00030\u0015\"\u0004\u0018\u00010\u0003H\u0014\u00a2\u0006\u0002\u0010\u0016J\u0017\u0010\u0017\u001a\u00020\r2\b\u0010\n\u001a\u0004\u0018\u00018\u0001H\u0014\u00a2\u0006\u0002\u0010\u0018RD\u0010\u0006\u001a8\u0012\u0015\u0012\u0013\u0018\u00018\u0001\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0015\u0012\u0013\u0018\u00010\u000b\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u00020\r\u0018\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\f\u001a\u0004\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012\u00a8\u0006\u0019"}, d2 = {"Lco/velloso/android/database/BaseDao$SimpleAsyncTask;", "R", "Landroid/os/AsyncTask;", "Ljava/lang/Void;", "onWorkerThread", "Lkotlin/Function0;", "onResult", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "result", "", "t", "", "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function2;)V", "getT", "()Ljava/lang/Throwable;", "setT", "(Ljava/lang/Throwable;)V", "doInBackground", "params", "", "([Ljava/lang/Void;)Ljava/lang/Object;", "onPostExecute", "(Ljava/lang/Object;)V", "library_release"})
    static final class SimpleAsyncTask<R extends java.lang.Object> extends android.os.AsyncTask<java.lang.Void, java.lang.Void, R> {
        @org.jetbrains.annotations.Nullable()
        private java.lang.Throwable t;
        private final kotlin.jvm.functions.Function0<R> onWorkerThread = null;
        private final kotlin.jvm.functions.Function2<R, java.lang.Throwable, kotlin.Unit> onResult = null;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Throwable getT() {
            return null;
        }
        
        public final void setT(@org.jetbrains.annotations.Nullable()
        java.lang.Throwable p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        protected R doInBackground(@org.jetbrains.annotations.NotNull()
        java.lang.Void... params) {
            return null;
        }
        
        @java.lang.Override()
        protected void onPostExecute(@org.jetbrains.annotations.Nullable()
        R result) {
        }
        
        public SimpleAsyncTask(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function0<? extends R> onWorkerThread, @org.jetbrains.annotations.Nullable()
        kotlin.jvm.functions.Function2<? super R, ? super java.lang.Throwable, kotlin.Unit> onResult) {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lco/velloso/android/database/BaseDao$Companion;", "", "()V", "TAG", "", "library_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}
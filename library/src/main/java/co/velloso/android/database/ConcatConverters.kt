package co.velloso.android.database

import androidx.room.TypeConverter

class ConcatConverters {
    @TypeConverter
    fun listIdsFromConcatString(value: String?): List<Long>? = value
            ?.split("^^")
            ?.map { it.toLong() }
            ?: emptyList()

    @TypeConverter
    fun listIdsToConcatString(value: List<Long>?): String? = value?.joinToString("^^")

    @TypeConverter
    fun listStringFromConcatString(value: String?): List<String>? = value
            ?.split("^^")
            ?: emptyList()

    @TypeConverter
    fun listStringToConcatString(value: List<String>?): String? = value?.joinToString("^^")
}
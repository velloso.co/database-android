package co.velloso.android.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class JsonConverters {

    @TypeConverter
    fun listIntFromJsonString(value: String?): List<Int>? {
        return if (value == null) null
        else Gson().fromJson(value, object : TypeToken<List<Int>>() {}.type)
    }

    @TypeConverter
    fun listIntToJsonString(value: List<Int>?): String? {
        return if (value == null) null else Gson().toJson(value)
    }

    @TypeConverter
    fun listLongFromJsonString(value: String?): List<Long>? {
        return if (value == null) null
        else Gson().fromJson(value, object : TypeToken<List<Long>>() {}.type)
    }

    @TypeConverter
    fun listLongToJsonString(value: List<Long>?): String? {
        return if (value == null) null else Gson().toJson(value)
    }

    @TypeConverter
    fun listBooleanFromJsonString(value: String?): List<Boolean>? {
        return if (value == null) null
        else Gson().fromJson(value, object : TypeToken<List<Boolean>>() {}.type)
    }

    @TypeConverter
    fun listBooleanToJsonString(value: List<Boolean>?): String? {
        return if (value == null) null else Gson().toJson(value)
    }

    @TypeConverter
    fun listStringFromJsonString(value: String?): List<String>? {
        return if (value == null) null
        else Gson().fromJson(value, object : TypeToken<List<String>>() {}.type)
    }

    @TypeConverter
    fun listStringToJsonString(value: List<String>?): String? {
        return if (value == null) null else Gson().toJson(value)
    }

    @TypeConverter
    fun anyFromJsonString(value: String?): Any? {
        return if (value == null) null
        else Gson().fromJson(value, Any::class.java)
    }

    @TypeConverter
    fun anyToJsonString(value: Any?): String? {
        return if (value == null) null else Gson().toJson(value)
    }
}
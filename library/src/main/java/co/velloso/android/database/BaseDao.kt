package co.velloso.android.database

import android.os.AsyncTask
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE


@Dao
abstract class BaseDao<in T> {

    @Insert(onConflict = REPLACE)
    abstract fun insert(vararg entities: T): List<Long>

    @Update
    abstract fun update(vararg entities: T): Int

    @Transaction
    open fun save(vararg entities: T) = insert(*entities).mapIndexed { index, l ->
        if (l == -1L) {
            update(entities[index])
            null
        } else l
    }

    @Transaction
    open fun upsert(vararg entities: T) = insert(*entities).mapIndexed { index, l ->
        if (l == -1L) {
            update(entities[index])
            null
        } else l
    }

    @Delete
    abstract fun delete(vararg entities: T): Int

    fun insert(vararg entities: T, result: ((ids: List<Long>?) -> Unit)) {
        SimpleAsyncTask(
                onWorkerThread = { insert(*entities) },
                onResult = { it, _ -> result.invoke(it) }
        ).execute()
    }

    fun save(vararg entities: T, result: ((ids: List<Long?>?) -> Unit)) {
        SimpleAsyncTask(
                onWorkerThread = { save(*entities) },
                onResult = { it, _ -> result.invoke(it) }
        ).execute()
    }

    fun upsert(vararg entities: T, result: ((ids: List<Long?>?) -> Unit)) {
        SimpleAsyncTask(
                onWorkerThread = { upsert(*entities) },
                onResult = { it, _ -> result.invoke(it) }
        ).execute()
    }

    fun update(vararg entities: T, result: ((rowsUpdated: Int?) -> Unit)) {
        SimpleAsyncTask(
                onWorkerThread = { update(*entities) },
                onResult = { it, _ -> result.invoke(it) }
        ).execute()
    }

    fun delete(vararg entities: T, result: ((rowsDeleted: Int?) -> Unit)) {
        SimpleAsyncTask(
                onWorkerThread = { delete(*entities) },
                onResult = { it, _ -> result.invoke(it) }
        ).execute()
    }

    private class SimpleAsyncTask<R>(
            private val onWorkerThread: () -> R?,
            private val onResult: ((result: R?, t: Throwable?) -> Unit)? = null

    ) : AsyncTask<Void, Void, R?>() {

        var t: Throwable? = null

        override fun doInBackground(vararg params: Void?): R? {
            return try {
                onWorkerThread.invoke()
            } catch (e: Throwable) {
                this.t = e
                null
            }
        }

        override fun onPostExecute(result: R?) {
            onResult?.invoke(result, t)
        }
    }

    companion object {
        const val TAG = "BaseDao"
    }
}